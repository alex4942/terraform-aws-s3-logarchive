resource "aws_s3_bucket" "vpc_log" {
  bucket = var.vpc_log_bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  lifecycle_rule {
    enabled = true
    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
}
resource "aws_s3_bucket_policy" "vpc_log" {
  bucket = aws_s3_bucket.vpc_log.id
  policy = data.aws_iam_policy_document.vpc_log.json
}

