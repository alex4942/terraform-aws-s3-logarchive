variable "config_bucket_name" {
  description = "bucket name"
  type        = string
  default     = "TUW2-SECLOG-S3-CONFIG"
}

variable "security_hub_bucket_name" {
  description = "bucket name"
  type        = string
  default     = "TUW2-SECLOG-S3-SECHUB"
}

variable "vpc_log_bucket_name" {
  description = "bucket name"
  type        = string
  default     = "TUW2-SECLOG-S3-VPCLOG"
}

variable "cloudtrail_bucket_name" {
  description = "bucket name"
  type        = string
  default     = "TUW2-SECLOG-S3-TRAIL"
}

/*variable "provider_account_id" {
  description = "provider account id"
  type        = string
}
variable "aws_provider_assume_role_name" {
  description = "aws provider role name"
  type        = string
}

variable "backend_account_id" {
  description = "s3 backend acc id"
  type        = string
  default     = ""
}

variable "aws_backend_assume_role_name" {
  description = "s3 backend assume role name"
  type        = string
  default     = ""
}
*/




