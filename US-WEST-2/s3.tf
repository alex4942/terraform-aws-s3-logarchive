resource "aws_s3_bucket" "config" {
  bucket = var.config_bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  lifecycle_rule {
    enabled = true
    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
}

resource "aws_s3_bucket_policy" "config" {
  bucket = aws_s3_bucket.config.id
  policy = data.aws_iam_policy_document.config.json
}

resource "aws_s3_bucket" "security_hub" {
  bucket = var.security_hub_bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  lifecycle_rule {
    enabled = true
    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
}

#resource "aws_s3_bucket_policy" "security_hub" {
# bucket = aws_s3_bucket.security_hub.id
#policy = data.aws_iam_policy_document.security_hub.json
#}

resource "aws_s3_bucket" "vpc_log" {
  bucket = var.vpc_log_bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  lifecycle_rule {
    enabled = true
    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
}
resource "aws_s3_bucket_policy" "vpc_log" {
  bucket = aws_s3_bucket.vpc_log.id
  policy = data.aws_iam_policy_document.vpc_log.json
}

resource "aws_s3_bucket" "cloudtrail" {
  bucket = var.cloudtrail_bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
/*  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.cloudtrail.id
        sse_algorithm     = "aws:kms"
      }
    }
  }*/
  lifecycle_rule {
    enabled = true
    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
}

resource "aws_s3_bucket_policy" "cloudtrail" {
  bucket = aws_s3_bucket.cloudtrail.id
  policy = data.aws_iam_policy_document.cloudtrail.json
}
