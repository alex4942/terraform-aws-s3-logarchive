resource "aws_s3_bucket_public_access_block" "vpc_log" {
  bucket                  = aws_s3_bucket.vpc_log.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

