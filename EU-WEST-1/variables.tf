variable "vpc_log_bucket_name" {
  description = "bucket name"
  type        = string
  default     = "TEW1-SECLOG-S3-VPCLOG"
}

variable "aws_region" {
  description = "bucket region"
  type        = string
}

/*variable "provider_account_id" {
  description = "provider account id"
  type        = string
}
variable "aws_provider_assume_role_name" {
  description = "aws provider role name"
  type        = string
}

variable "backend_account_id" {
  description = "s3 backend acc id"
  type        = string
  default     = ""
}

variable "aws_backend_assume_role_name" {
  description = "s3 backend assume role name"
  type        = string
  default     = ""
}
*/




